(function ($) {

  Drupal.behaviors.harlemshake = {
    attach: function (context, settings) {
      $('#harlemshake_button').click(function () {
        if (Drupal.settings.harlemshake.clicked != 1) {
          Drupal.settings.harlemshake.clicked = 1;
          $('#harlemshake_button').effect('shake', {times:145 }, 100);
          var bounce = Drupal.settings.harlemshake.bounce;
          var shake = Drupal.settings.harlemshake.shake;
          var pulsate = Drupal.settings.harlemshake.pulsate;
          setTimeout(function() {
            $.each(bounce, function(i, val) {
              $(val).not('#harlemshake_button').effect('shake', {direction:'up',times:75}, 100);
            });
            $.each(shake, function(i, val) {
              $(val).not('#harlemshake_button').effect('shake', {times:37}, 200);
            });
            $.each(pulsate, function(i, val) {
              $(val).not('#harlemshake_button').effect("pulsate", {times:12}, 1500);
            });

          }, 15500);
          if (Modernizr.audio.mp3) {
            var audio = document.getElementById('harlemshake_mp3');  
          }
          else if (Modernizr.audio.ogg) {
            var audio = document.getElementById('harlemshake_ogg');  
          }
          audio.play();
          setTimeout(function() {
            Drupal.settings.harlemshake.clicked = 0;
            audio.pause();
          }, 33000);
        }
      });
    }
  };

})(jQuery);
